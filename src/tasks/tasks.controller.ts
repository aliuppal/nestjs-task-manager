import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatusValidationPipe } from './dto/pipes/task-status-validation.pipe';
import { TaskStatus } from './task-status.enum';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUserInfo } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  private logger = new Logger('TasksController');
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(
    @Query(ValidationPipe) filtertDto: GetTasksFilterDto,
    @GetUserInfo() user: User,
  ) {
    this.logger.verbose(`User ${user.username} retrieving all tasks. Filters: ${JSON.stringify(filtertDto)}`);
    return this.tasksService.getTasks(filtertDto, user);
  }

  @Get('/:id')
  getTaskById(
    @Param('id', ParseIntPipe) id: number,
    @GetUserInfo() user: User,
  ) {
    this.logger.verbose(`User ${user.username} request for task ${id}`);
    return this.tasksService.getTaskById(id, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createTask(
    @Body() createTaskDto: CreateTaskDto,
    @GetUserInfo() user: User,
  ): Promise<Task> {
    this.logger.verbose(`User ${user.username} create a new task ${JSON.stringify(createTaskDto)}`);
    return this.tasksService.createTask(createTaskDto, user);
  }

  @Patch('/:id/status')
  updateTaskStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', TaskStatusValidationPipe) status: TaskStatus,
    @GetUserInfo() user: User,
  ) {
    this.logger.verbose(`User ${user.username} request for update task ${id} with status ${status}`);
    return this.tasksService.updateTaskStatus(id, status, user);
  }

  @Delete('/:id')
  deleteTaskById(
    @Param('id', ParseIntPipe) id: number,
    @GetUserInfo() user: User,
  ): Promise<void> {
    this.logger.verbose(`User ${user.username} request for delete task ${id}`);
    return this.tasksService.deleteTaskById(id, user);
  }
}
