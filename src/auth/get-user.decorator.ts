import { createParamDecorator } from '@nestjs/common';
import { User } from './user.entity';

export const GetUserInfo = createParamDecorator((data, context): User => {
  const req = context.switchToHttp().getRequest();
  return req.user;
});
