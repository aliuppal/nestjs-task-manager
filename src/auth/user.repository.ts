import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { LoginDto } from './dto/login.dto';
import { SignUpDto } from './dto/signup.dto';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(signUpDto: SignUpDto): Promise<void> {
    const { username, password } = signUpDto;
    const user = new User();
    user.username = username;
    user.salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, user.salt);
    try {
      await user.save();
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('Username already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async login(loginDto: LoginDto): Promise<void> {
    const { username, password } = loginDto;
    const user = new User();
    user.username = username;
    // user.password = await this.hashPassword(password);
    await user.save();

    console.log(user);
  }

  async validateUserPassowrd(loginDto: LoginDto): Promise<string> {
    const { username, password } = loginDto;
    const user = await this.findOne({ username });
    if (user && (await user.validatePassowrd(password))) {
      return username;
    } else {
      return null;
    }
  }

  private async hashPassword(passowrd: string, salt: string) {
    return await bcrypt.hash(passowrd, salt);
  }
}
