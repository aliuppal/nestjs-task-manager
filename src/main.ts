import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  // add log module
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);

  if (process.env.USE_CORES) {
    app.enableCors();
  }

  const port = process.env.PORT || 3000;
  await app.listen(port);

  logger.log(`Application listening on port: ${port}`);
}
bootstrap();
